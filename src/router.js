import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/video/:videoId',
      name: 'video',
      // route level code-splitting
      // this generates a separate chunk (video.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      props: (route) => ({ videoId: route.params.videoId }),
      component: () => import(/* webpackChunkName: "video" */ './views/Video.vue')
    },
    {
      path: '/search/:searchTerm',
      name: 'search',
      props: (route) => ({ searched: route.params.searchTerm }),
      component: () => import(/* webpackChunkName: "search" */ './views/Search.vue'),
    }
  ]
})
